import React, {Component} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import firebase from './../../firebase.js';
import CONFIG from "../../configs/config";

export default class HabitCheckbox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id,
            day: this.props.day,
            checked: this.props.checked,
            section: this.props.section,
            disabled: this.props.disabled,
        };

        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }

    handleCheckboxChange(el, newState) {
        firebase.database().ref(CONFIG.getHabitsURL(this.state.section) + '/' + this.state.id + '/days/' + this.state.day).set({
            checked: newState
        });
        this.setState({checked: newState});
    }

    render() {
        return (
            <Checkbox
                checked={this.state.checked}
                disabled={this.state.disabled}
                onChange={this.handleCheckboxChange.bind(this)}
                color="primary"
            />
        );
    }
}