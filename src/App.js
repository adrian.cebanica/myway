import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Dashboard from "./sections/Dashboard/Dashboard";

function App() {
    return (
        <Router>
            <div>
                <Route exact path='/' component={Dashboard}/>
            </div>
        </Router>
    );
}

export default App;
