import moment from "moment";

const CONFIG = {};

CONFIG.currentMonth = moment().month() + 1;
CONFIG.selectedMonth = CONFIG.currentMonth;
CONFIG.currentMonthLabel = moment().format("MMMM");
CONFIG.currentYear = Number(moment().format('YYYY'));
CONFIG.daysInMonth = moment().daysInMonth();

CONFIG.getHabitsURL = function (section) {
   if(!section) {
      console.error('getHabitsURL has empty param');
   }
   return  '/habits/' + CONFIG.currentYear + '/' + CONFIG.selectedMonth + '/' + section;
};

export default CONFIG;