/* eslint-disable no-script-url */
import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import firebase from "./../../firebase";
import CONFIG from "../../configs/config";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';

export default class HabitDelete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            title: props.title,
            rowId: props.rowId,
            section: props.section
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    updateInputValue = (evt) => {
        this.setState({
            newTitle: evt.target.value
        });
    };

    onSubmit = (evt) => {
        if(!this.state.rowId) {
            return false;
        }

        firebase.database()
            .ref(CONFIG.getHabitsURL(this.state.section) + '/' + this.state.rowId)
            .remove();

        this.handleClose();
    };

    render() {
        return (
            <div>
                <IconButton
                    aria-label="delete"
                    onClick={this.handleClickOpen.bind(this)}>
                    <DeleteIcon
                    />
                </IconButton>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">Confirm habit delete?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Delete habit "{this.state.title}"
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmit} color="primary">
                            Confirm
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}