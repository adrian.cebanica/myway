/* eslint-disable no-script-url */
import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import firebase from "./../../firebase";
import CONFIG from "../../configs/config";
import {Box} from "@material-ui/core";

export default class HabitAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            newTitle: '',
            section: props.section
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    updateInputValue = (evt) => {
        this.setState({
            newTitle: evt.target.value
        });
    };

    onSubmit = (evt) => {
        let days = [];
        for (let i = 1; i <= CONFIG.daysInMonth; i++) {
            days[i] = {
                checked: false,
            }
        }

        firebase.database()
            .ref(CONFIG.getHabitsURL(this.state.section))
            .push({
                title: this.state.newTitle,
                days: days
            });

        this.handleClose();
    };

    render() {
        return (
            <Box mt={2}>
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.handleClickOpen.bind(this)}>
                    Add Habit
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose.bind(this)} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Create Habit</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Choose a title.
                            The new habit will be added to the current month.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Habit Title"
                            type="habit"
                            fullWidth
                            onChange={evt => this.updateInputValue(evt)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose.bind(this)} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmit.bind(this)} color="primary">
                            Create
                        </Button>
                    </DialogActions>
                </Dialog>
            </Box>
        );
    }
}