/* eslint-disable no-script-url */
import React, {Component} from 'react';
// import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import HabitCheckbox from "./HabitCheckbox";
import firebase from './../../firebase.js';
import CONFIG from "../../configs/config";
import HabitDelete from "./HabitDelete";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import moment from "moment";
import {withStyles} from "@material-ui/core/styles";

const styles = theme => ({
    cells: {
        padding: 0,
        minWidth: 65,
    },
    cellTitle: {
        minWidth: 200,
        padding: 0,
    },
    deleteButton: {
        padding: 0,
    }
});

class HabitTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            section: props.section,
            currentYear: CONFIG.currentYear,
            selectedMonth: CONFIG.selectedMonth,
            daysInMonth: CONFIG.daysInMonth,
            today: moment().date()
        };

        this.getHabits();
    }

    // Generate Order Data
    getHabits() {
        const itemsRef = firebase.database().ref(CONFIG.getHabitsURL(this.state.section));

        itemsRef.on('value', (snapshot) => {
            let items = snapshot.val();
            let newState = [];

            console.log(items);
            for (let item in items) {
                if (!items.hasOwnProperty(item)) {
                    continue;
                }

                newState.push({
                    id: item,
                    title: items[item].title,
                    days: items[item].days
                });
            }
            this.setState({
                rows: newState
            });
        });
    }

    Header() {
        const {classes} = this.props;

        let days = [];
        for (let d = 1; d <= this.state.daysInMonth; d++) {
            let current_date = moment(this.state.currentYear + '-' + this.state.selectedMonth + '-' + d, 'YYYY-MM-DD')
                .format('ddd D');
            days.push({
                id: d,
                label: current_date
            });
        }

        return (
            <TableHead>
                <TableRow>
                    <TableCell className={classes.cells} key='delete'> </TableCell>
                    <TableCell className={classes.cellTitle} key='habits'> </TableCell>

                    {days.map(day => (
                        <TableCell className={classes.cells} key={day.id}>
                            <Typography component="div">
                                <Box fontWeight={day.id === this.state.today ? "fontWeightBold" : ''}>
                                    {day.label}
                                </Box>
                            </Typography>
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
        );
    }

    Body() {
        const {classes} = this.props;

        return (
            <TableBody>
                {this.state.rows.map(row => (
                    <TableRow key={row.id}>
                        <TableCell className={classes.cells}>
                            <HabitDelete
                                className={classes.deleteButton}
                                section={this.state.section}
                                rowId={row.id}
                                title={row.title}/>
                        </TableCell>
                        <TableCell className={classes.cellTitle}>
                            <Typography component="div">
                                <Box fontWeight="fontWeightBold">
                                    {row.title}
                                </Box>
                            </Typography>
                        </TableCell>
                        {Object.keys(row.days).map((dayId) => (
                            <TableCell key={dayId} className={classes.cells}>
                                <HabitCheckbox
                                    section={this.state.section}
                                    checked={row.days[dayId].checked}
                                    disabled={dayId < this.state.today}
                                    day={dayId}
                                    id={row.id}
                                />
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
            </TableBody>
        );
    }

    render() {
        return (
            <React.Fragment>
                <Table size={"small"}>
                    {this.Header()}
                    {this.Body()}
                </Table>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(HabitTable);