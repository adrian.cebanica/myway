import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CONFIG from "../../configs/config";

const useStyles = makeStyles(() =>
    createStyles({
        formControl: {
            // margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            // marginTop: theme.spacing(2),
        },
    }),
);

export default function HabitMonthSelect() {
    const classes = useStyles();
    const [month, setMonth] = React.useState(CONFIG.currentMonth);

    const handleChange = (ev) => {
        CONFIG.selectedMonth = ev.target.value;
        setMonth(ev.target.value);
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="month-select-label">Month</InputLabel>
                <Select
                    labelId="month-select-label"
                    id="month-select"
                    value={month}
                    onChange={handleChange}
                >
                    <MenuItem value={1}>January</MenuItem>
                    <MenuItem value={2}>February</MenuItem>
                    <MenuItem value={3}>March</MenuItem>
                    <MenuItem value={4}>April</MenuItem>
                    <MenuItem value={5}>May</MenuItem>
                    <MenuItem value={6}>June</MenuItem>
                    <MenuItem value={7}>July</MenuItem>
                    <MenuItem value={8}>August</MenuItem>
                    <MenuItem value={9}>September</MenuItem>
                    <MenuItem value={10}>October</MenuItem>
                    <MenuItem value={11}>November</MenuItem>
                    <MenuItem value={12}>December</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}
