MyWay - small app that will help you organize tasks, habits, events.  
https://myway-f5fcc.firebaseapp.com/  
https://myway13.herokuapp.com/

--- 

1. `git add -A && git commit -m "update" && git push`

2. `npm run build && firebase deploy`

--- 

- [ ] Create authentication
- [ ] Move credentials to .env file
- [ ] Find a way to link sidebar list to pages
- [ ] Setup firebase database
- [ ] Setup CRUD for habits
- [ ] Setup habits
- [ ] Add possibility to rearrange, edit, delete habits
